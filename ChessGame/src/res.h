#pragma once
#include "oxygine-framework.h"
#include <string>
using namespace oxygine;

namespace res
{
    extern Resources ui;
    void load();
    void free();
}
