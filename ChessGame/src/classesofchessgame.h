#ifndef CLASSESOFCHESSGAME_H
#define CLASSESOFCHESSGAME_H
#include "oxygine-framework.h"
#include "res.h"
#include <iostream>
#include <vector>
#include <stdint.h>
using namespace oxygine;
using namespace std;
enum Side {White, Black};
enum Piece {QUEEN, KING, BISHOP, KNIGHT, ROOK, PAWN};
class Game; class Figure; class Queen; class King;
class Bishop; class Knight; class Rook; class Pawn;


class Game {
private:
    Game() : nextPiece(White) {}
    Game(const Game&);
    Game& operator =(const Game&);
    Color oldColorBoard[8][8];

public:
    Figure* figure[32];
    Side nextPiece;
    struct ElementOfBoard
    {
      spSprite element;
      bool empty;
    } board[8][8];
    ~Game();
    static Game& getObject();
    void returnOldColorBoard();
    void newGame();
    void drawBoard();
    void drawChess();
    void eventTouchBoard();
    void eventTouchFigure();
    void emptyElementsOfBoard ();
};


class Figure {
protected:
    Side side;
    int xFigure, yFigure;
    bool atack, press;
    vector<Point> allowingPointsToAtackVar;
    void handlerEventTouchFigure(Figure* figure);
public:
    Figure() {}
    virtual ~Figure() {}
    virtual void load_sprite(float x, float y, const Side& side) = 0; //загрузка спрайта для фигуры
    virtual bool moveTO(int x, int y) = 0; //Ход
    virtual Side& getSide()  = 0; //возвращает цвет стороны фигуры
    virtual spSprite& getSprite() = 0; //Возвращает спрайт фигуры
    virtual void eventTouchFigure() = 0; //События, возникающие при клике по фигуре
    virtual Point getIndexFigure() = 0; //возвращает текущий индекс фигуры
    virtual void setCheckAtack(bool atack) = 0;
    virtual bool atacked() = 0; //возвращает true, если фигура была атакована
    virtual bool& pressed() = 0; //Возвращает true, если Фигура зажата кликом
    virtual vector<Point>  getAllowingPointsToMove() = 0; //возвращает точки, по которым фигура может передвинуться
    virtual vector<Point>& getAllowingPointsToAtack() = 0; //возвращает точки, по которым фигура может передвинуться, съев противника
    virtual Piece getPiece() = 0; //возвращает название фигуры
    virtual void setCheckFirstMove(bool isMoveCheck) = 0;
};

class Queen : public Figure {
private:
    spSprite spriteQueen;
public:
     Queen() { this->atack = this->press = false; }
     ~Queen() {}
     Point getIndexFigure() override;
     void load_sprite(float x, float y, const Side& side) override;
     bool moveTO(int x, int y) override;
     void eventTouchFigure() override;
     Side& getSide() override;
     spSprite& getSprite() override;
     void setCheckAtack(bool atack) override;
     bool atacked() override;
     bool& pressed() override;
     vector<Point> getAllowingPointsToMove() override;
     vector<Point>& getAllowingPointsToAtack() override;
     Piece getPiece() override;
     void setCheckFirstMove(bool isMoveCheck) {}
};

class King : public Figure {
private:
    spSprite spriteKing;
public:
    King() {this->atack = this->press = false;}
    ~King() {}
    void load_sprite(float x, float y, const Side& side) override;
    bool moveTO(int x, int y) override;
    void eventTouchFigure() override;
    Side& getSide() override;
    spSprite& getSprite() override;
    Point getIndexFigure() override;
    void setCheckAtack(bool atack) override;
    bool atacked() override;
    bool& pressed() override;
    vector<Point> getAllowingPointsToMove() override;
    vector<Point>& getAllowingPointsToAtack() override;
    Piece getPiece() override;
    void setCheckFirstMove(bool isMoveCheck) {}
};

class Bishop : public Figure {
private:
    spSprite spriteBishop;
    int touchedUpX, touchedUpY, touchedDownX, touchedDownY;
public:
    Bishop()  {this->atack = this->press = false;}
    ~Bishop() {}
    void load_sprite(float x, float y, const Side& side) override;
    bool moveTO(int x, int y) override;
    void eventTouchFigure() override;
    Side& getSide() override;
    spSprite& getSprite() override;
    Point getIndexFigure() override;
    void setCheckAtack(bool atack) override;
    bool atacked() override;
    bool& pressed() override;
    vector<Point> getAllowingPointsToMove() override;
    vector<Point>& getAllowingPointsToAtack() override;
    Piece getPiece() override;
    void setCheckFirstMove(bool isMoveCheck) {}
};

class Rook : public Figure {
private:
    spSprite spriteRook;
public:
    Rook()  {this->atack = this->press = false;}
    ~Rook() {}
    void load_sprite(float x, float y, const Side& side) override;
    bool moveTO(int x, int y) override;
    void eventTouchFigure() override;
    Side& getSide() override;
    spSprite& getSprite() override;
    Point getIndexFigure() override;
    void setCheckAtack(bool atack) override;
    bool atacked() override;
    bool& pressed() override;
    vector<Point> getAllowingPointsToMove() override;
    vector<Point>& getAllowingPointsToAtack() override;
    Piece getPiece() override;
    void setCheckFirstMove(bool isMoveCheck) {}
};


class Knight : public Figure {
private:
    spSprite spriteKnight;
public:
    Knight()  {this->atack = this->press = false;}
    ~Knight() {}
    void load_sprite(float x, float y, const Side& side) override;
    bool moveTO(int x, int y) override;
    void eventTouchFigure() override;
    Side& getSide() override;
    spSprite& getSprite() override;
    Point getIndexFigure() override;
    void setCheckAtack(bool atack) override;
    bool atacked() override;
    bool& pressed() override;
    vector<Point> getAllowingPointsToMove() override;
    vector<Point>& getAllowingPointsToAtack() override;
    Piece getPiece() override;
    void setCheckFirstMove(bool isMoveCheck) {}
};

class Pawn : public Figure {
private:
    spSprite spritePawn;
    bool isMoveCheck;
public:
    Pawn() : isMoveCheck(false) {this->press = false;}
    ~Pawn() {}
    void load_sprite(float x, float y, const Side& side) override;
    bool moveTO(int x, int y) override;
    void eventTouchFigure() override;
    Side& getSide() override;
    spSprite& getSprite() override;
    Point getIndexFigure() override;
    void setCheckAtack(bool atack) override;
    bool atacked() override;
    bool& pressed() override;
    vector<Point> getAllowingPointsToMove() override;
    vector<Point>& getAllowingPointsToAtack() override;
    Piece getPiece() override;
    void setCheckFirstMove(bool isMoveCheck) override;
};

#endif // CLASSESOFCHESSGAME_H
