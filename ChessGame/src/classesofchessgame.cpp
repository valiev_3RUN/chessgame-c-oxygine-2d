#include "classesofchessgame.h"

/*Singleton class Game*/
Game::Game(const Game &) {}
Game::~Game() {
    for (uint8_t i = 0; i < 32; i++) {
        delete figure[i];
    }
}

void Game::newGame() {
    drawBoard();
    eventTouchBoard();
    drawChess();
    eventTouchFigure();
    emptyElementsOfBoard();
}

void Game::eventTouchFigure() {
    for (uint8_t i = 0; i < 32; i++) {
        figure[i]->eventTouchFigure();
    }
}
void Game::drawChess() {
    int x = 0;
    for (uint8_t i = 0; i <16; i++) {
         figure[i] = new Pawn;
        if (i < 8) {
            figure[i]->load_sprite(x, 90, Black);
            x += 45;
        } else
            figure[i]->load_sprite(x -= 45, 315, White);
    }
    figure[16] = new Rook; figure[16]->load_sprite(0, 45, Black);
    figure[17] = new Knight; figure[17]->load_sprite(45,45,  Black);
    figure[18] = new Bishop;   figure[18]->load_sprite(90,45,  Black);
    figure[19] = new Queen;  figure[19]->load_sprite(135, 45, Black);
    figure[20] = new King; figure[20]->load_sprite(180, 45, Black);
    figure[21] = new Bishop;figure[21]->load_sprite(225, 45, Black);
    figure[22] = new Knight; figure[22]->load_sprite(270, 45, Black);
    figure[23] = new Rook;  figure[23]->load_sprite(315, 45, Black);
    figure[24] = new Rook;  figure[24]->load_sprite(0,   360,  White);
    figure[25] = new Knight;  figure[25]->load_sprite(45, 360, White);
    figure[26] = new Bishop; figure[26]->load_sprite(90, 360, White);
    figure[27] = new Queen;figure[27]->load_sprite(135, 360, White);
    figure[28] = new King;figure[28]->load_sprite(180, 360,  White);
    figure[29] = new Bishop; figure[29]->load_sprite(225, 360, White);
    figure[30] = new Knight; figure[30]->load_sprite(270, 360, White);
    figure[31] = new Rook; figure[31]->load_sprite(315, 360, White);
}

void Game::drawBoard() {
    res::load();
    bool check = false;
    float x = 0, y = 45;
    for (uint8_t i = 0; i < 8; i++) {
        for (uint8_t j = 0; j < 8; j++) {
            board[i][j].element = new Sprite;
            if (!check)
                 board[i][j].element->setResAnim(res::ui.getResAnim("whitesquare"));
            else
                 board[i][j].element->setResAnim(res::ui.getResAnim("blacksquare"));
            board[i][j].element->attachTo(getStage());
            board[i][j].element->setPosition(x, y);
            oldColorBoard[i][j] = board[i][j].element->getColor();
           x += 45;
           check = !check;
        }
        check = !check;
        y += 45;
        x = 0;
    }
}
void Game::eventTouchBoard() {
    for (uint8_t x = 0; x < 8; x++) {
        for (uint8_t y = 0; y < 8; y++) {
            board[x][y].element->addEventListener(TouchEvent::TOUCH_UP, [=](Event* ev) {
                  this->returnOldColorBoard();
                  for (uint8_t k = 0; k < 32; k++) {
                      if (figure[k]->pressed() && figure[k]->getSide() == this->nextPiece) {
                          vector<Point> v = figure[k]->getAllowingPointsToMove();
                          for (uint8_t i = 0; i < v.size(); i++) {
                              if (x == v[i].x && y == v[i].y) {
                                  figure[k]->getSprite()->addTween(Actor::TweenX(board[x][y].element->getPosition().x), 200);
                                  figure[k]->getSprite()->addTween(Actor::TweenY(board[x][y].element->getPosition().y), 200);
                                  if (figure[k]->getPiece() == Piece::PAWN) figure[k]->setCheckFirstMove(true);
                                  if (nextPiece == White) nextPiece = Black; else nextPiece = White;
                              }
                          }
                          this->returnOldColorBoard();
                      }
                  }
            });
            board[x][y].element->addEventListener(TouchEvent::MOVE, [=](Event* ev) {
                for (uint8_t k = 0; k < 32; k++) {
                    if (figure[k]->pressed() && figure[k]->getSide() == this->nextPiece) {
                        vector<Point> v = figure[k]->getAllowingPointsToAtack();
                        for (uint8_t i = 0; i < v.size(); i++) {
                            if (x == v[i].x && y == v[i].y) {
                                for (uint8_t j = 0; j < 32; j++){
                                    int tempX = figure[j]->getIndexFigure().x,
                                        tempY = figure[j]->getIndexFigure().y;
                                    if (tempX == x && tempY == y) {
                                        if (figure[j]->getSide() == White)
                                            figure[j]->getSprite()->addTween(Actor::TweenY(0),150);
                                         else
                                            figure[j]->getSprite()->addTween(Actor::TweenY(405), 150);
                                        figure[j]->getSprite()->addTween(Actor::TweenX(figure[j]->getSprite()->getPosition().x), 150);
                                        figure[j]->setCheckAtack(true);
                                        this->returnOldColorBoard();
                                    }
                                }
                                figure[k]->getSprite()->addTween(Actor::TweenX(board[x][y].element->getPosition().x), 150);
                                figure[k]->getSprite()->addTween(Actor::TweenY(board[x][y].element->getPosition().y), 150);
                                if (figure[k]->getPiece() == Piece::PAWN) figure[k]->setCheckFirstMove(true);
                                if (nextPiece == White) nextPiece = Black; else nextPiece = White;
                            }
                        }
                    }
                }

            });
        }
    }
}


Game& Game::getObject() {
    static Game object;
    return object;
}

void Game::emptyElementsOfBoard() {
    for (uint8_t i = 0; i < 8; i++) {
        for (uint8_t j = 0; j < 8; j++) {
            board[i][j].empty = true;
        }
    }
    for (uint8_t i = 0; i < 32; i++) {
        if (!figure[i]->atacked()) {
            int X = figure[i]->getIndexFigure().x,
                Y = figure[i]->getIndexFigure().y;
            board[X][Y].empty = false;
        }
    }
}
void Game::returnOldColorBoard() {
    for (uint8_t i = 0; i < 8; i++) {
        for (uint8_t j = 0; j < 8; j++) {
            board[i][j].element->setColor(oldColorBoard[i][j]);
        }
    }
}

void Figure::handlerEventTouchFigure(Figure* figure) {
    figure->getSprite()->addEventListener(TouchEvent::TOUCH_DOWN, [=](Event* ev) {
        if (!figure->atacked()) {
            Game::getObject().returnOldColorBoard();
            if (!figure->getAllowingPointsToAtack().empty()) figure->getAllowingPointsToAtack().clear();
            vector<Point> vecMove =  figure->getAllowingPointsToMove();
            if (!figure->getAllowingPointsToAtack().empty() && figure->getSide() == Game::getObject().nextPiece) {
                figure->pressed() = true;
                for (int i = 0; i < figure->getAllowingPointsToAtack().size(); i++) {
                        Game::getObject().board[figure->getAllowingPointsToAtack()[i].x][figure->getAllowingPointsToAtack()[i].y].element->setColor(200,32,32,200);
                }
            }

            if (!vecMove.empty() && figure->getSide() == Game::getObject().nextPiece) {
                figure->pressed() = true;
                for (int i = 0; i < vecMove.size(); i++) {
                    Game::getObject().board[vecMove[i].x][vecMove[i].y].element->setColor(200,32,32,200); //32, 32, 32, 100
                }
            }
        }
    });

    figure->getSprite()->addEventListener(TouchEvent::TOUCH_UP, [=](Event* ev) {
        figure->pressed() = false;
    });
}

//////////////////////////////////////////////////////////////////////
////////////////////////////////*Королева*//////////////////////////////
/////////////////////////////////////////////////////////////////////
Point Queen::getIndexFigure() {
    Point p;
    for (uint16_t i = 0, y = 0; i <= 315; i += 45, y++) {
        for (uint16_t j = 45, x = 0; j <=360; j += 45, x++ ) {
            if (this->getSprite()->getPosition().x == i && this->getSprite()->getPosition().y == j) {
                 p.x  = this->xFigure = x; p.y  = this->yFigure = y;
            }
        }
    }
    return p;
}
bool& Queen::pressed() {
    return this->press;
}
void Queen::load_sprite(float x, float y, const Side& side)  {
    this->side = side;
    spriteQueen = new Sprite;
    if (side == White)
       spriteQueen->setResAnim(res::ui.getResAnim("WQueen"));
    else
       spriteQueen->setResAnim(res::ui.getResAnim("BQueen"));
    spriteQueen->attachTo(getStage());
    spriteQueen->setPosition(x, y);
}
vector<Point>& Queen::getAllowingPointsToAtack() {
    return this->allowingPointsToAtackVar;
}

vector<Point> Queen::getAllowingPointsToMove() {
    vector<Point> vecTemp;
    Point p;
    int posX = this->getIndexFigure().x,
        posY = this->getIndexFigure().y;
    Game::getObject().emptyElementsOfBoard();
    bool flag = true;

    auto gettingPointsToMove = [](Queen& figure, bool& flag, Point& p,  int& paramX, int& paramY) {
        if (!Game::getObject().board[paramX][paramY].empty) {
            for (uint8_t k = 0; k < 32; k++) {
                int tempX = Game::getObject().figure[k]->getIndexFigure().x,
                    tempY = Game::getObject().figure[k]->getIndexFigure().y;
                if (tempX == paramX && tempY == paramY && Game::getObject().figure[k]->getSide() == figure.getSide() ) flag = false;
                if (tempX == paramX && tempY == paramY && Game::getObject().figure[k]->getSide() != figure.getSide() && Game::getObject().figure[k]->getPiece() != Piece::KING ) {
                    p.x = tempX; p.y = tempY;
                    figure.allowingPointsToAtackVar.push_back(p);
                    flag = false;
                }
            }
        }
    };
    for (int x = posX - 1; x >= 0 && flag; x--) {
        gettingPointsToMove(*this, flag, p, x, posY);
    }
    flag = true;
    for (int x = posX + 1; x < 8 && flag; x++) {
        gettingPointsToMove(*this, flag, p, x, posY);
    }
    flag = true;
    for (int y = posY - 1; y >= 0 && flag; y--) {
        gettingPointsToMove(*this, flag, p, posX, y);
    }
    flag = true;
    for (int y = posY + 1; y < 8 && flag; y++) {
        gettingPointsToMove(*this, flag, p, posX, y);
    }

    flag = true;
    for (int x  = posX + 1, y = posY + 1; x < 8 && x >= 0 && y < 8 && y >= 0 && flag; x++, y++) {
        gettingPointsToMove(*this, flag, p, x, y);
    }
    flag = true;
    for (int x  = posX - 1, y = posY - 1; x < 8 && x >= 0 && y < 8 && y >= 0 && flag; x--, y--) {
        gettingPointsToMove(*this, flag, p, x, y);
    }
    flag = true;
    for (int x  = posX + 1, y = posY - 1; x < 8 && x >= 0 && y < 8 && y >= 0 && flag; x++, y--) {
        gettingPointsToMove(*this, flag, p, x, y);
    }
    flag = true;
    for (int x  = posX - 1, y = posY + 1; x < 8 && x >= 0 && y < 8 && y >= 0 && flag; x--, y++) {
        gettingPointsToMove(*this, flag, p, x, y);
    }
    for (int x  = posX + 1, y = posY + 1; x < 8 && x >= 0 && y < 8 && y >= 0 && Game::getObject().board[x][y].empty; x++, y++) {
        p.x = x; p.y = y;
        vecTemp.push_back(p);
    }
    for (int x  = posX - 1, y = posY - 1; x < 8 && x >= 0 && y < 8 && y >= 0 && Game::getObject().board[x][y].empty;  x--, y--) {
        p.x = x; p.y = y;
        vecTemp.push_back(p);
    }
    for (int x  = posX + 1, y = posY - 1; x < 8 && x >= 0 && y < 8 && y >= 0 && Game::getObject().board[x][y].empty; x++, y--) {
        p.x = x; p.y = y;
        vecTemp.push_back(p);
    }
    for (int x  = posX - 1, y = posY + 1; x < 8 && x >= 0 && y < 8 && y >= 0 && Game::getObject().board[x][y].empty; x--, y++) {
        p.x = x; p.y = y;
        vecTemp.push_back(p);
    }

    for (int x = posX - 1; x >= 0 && Game::getObject().board[x][posY].empty; x-- ) {
        p.x = x; p.y = posY;
        vecTemp.push_back(p);
    }
    for (int x = posX + 1; x < 8 && Game::getObject().board[x][posY].empty;  x++ ) {
        p.x = x; p.y = posY;
        vecTemp.push_back(p);
    }
    for (int y = posY - 1; y >= 0 && Game::getObject().board[posX][y].empty; y-- ) {
        p.x = posX; p.y = y;
        vecTemp.push_back(p);
    }
    for (int y = posY + 1; y <  8 && Game::getObject().board[posX][y].empty; y++ ) {
        p.x = posX; p.y = y;
        vecTemp.push_back(p);
    }
    return vecTemp;
}

void Queen::eventTouchFigure() {
    handlerEventTouchFigure(this);
}

bool Queen::moveTO(int x, int y) {

}
Piece Queen::getPiece() {
    return Piece::QUEEN;
}
Side& Queen::getSide() {
    return this->side;
}
spSprite& Queen::getSprite() {
    return spriteQueen;
}

void Queen::setCheckAtack(bool atack) {
    this->atack = atack;
}
bool Queen::atacked() {
    return this->atack;
}
//////////////////////////////////////////////////////////////////////
////////////////////////////////*Король*//////////////////////////////
/////////////////////////////////////////////////////////////////////
Point King::getIndexFigure() {
    Point p;
    for (int i = 0, y = 0; i <= 315; i += 45, y++) {
        for (int j = 45, x = 0; j <=360; j += 45, x++ ) {
            if (this->getSprite()->getPosition().x == i && this->getSprite()->getPosition().y == j) {
                 p.x = this->xFigure = x; p.y = this->yFigure = y;
            }
        }
    }
    return p;
}


bool& King::pressed() {
    return this->press;
}
Piece King::getPiece() {
    return Piece::KING;
}

void King::load_sprite(float x, float y, const Side& side)  {
    this->side = side;
    spriteKing = new Sprite;
    if (side == White)
       spriteKing->setResAnim(res::ui.getResAnim("WKing"));
    else
       spriteKing->setResAnim(res::ui.getResAnim("BKing"));
    spriteKing->attachTo(getStage());
    spriteKing->setPosition(x, y);
}
vector<Point>& King::getAllowingPointsToAtack() {
    return this->allowingPointsToAtackVar;
}

vector<Point> King::getAllowingPointsToMove() {
    vector<Point> vecTemp;
    Point p;
    int posX = this->getIndexFigure().x,
        posY = this->getIndexFigure().y;
    Game::getObject().emptyElementsOfBoard();
    for (int k = 0; k < 32; k++) {
        int tempX = Game::getObject().figure[k]->getIndexFigure().x,
            tempY = Game::getObject().figure[k]->getIndexFigure().y;
        if (posX + 1 == tempX  && posY + 1 == tempY && Game::getObject().figure[k]->getSide() != this->getSide() && Game::getObject().figure[k]->getPiece() != Piece::KING ) {
            p.x = tempX; p.y = tempY;
            this->allowingPointsToAtackVar.push_back(p);
        }
        if (posX - 1 == tempX  && posY - 1 == tempY && Game::getObject().figure[k]->getSide() != this->getSide() && Game::getObject().figure[k]->getPiece() != Piece::KING ) {
            p.x = tempX; p.y = tempY;
            this->allowingPointsToAtackVar.push_back(p);
        }
        if (posX + 1 == tempX  && posY - 1 == tempY && Game::getObject().figure[k]->getSide() != this->getSide() && Game::getObject().figure[k]->getPiece() != Piece::KING ) {
            p.x = tempX; p.y = tempY;
            this->allowingPointsToAtackVar.push_back(p);
        }
        if (posX - 1 == tempX  && posY + 1 == tempY && Game::getObject().figure[k]->getSide() != this->getSide() && Game::getObject().figure[k]->getPiece() != Piece::KING ) {
            p.x = tempX; p.y = tempY;
            this->allowingPointsToAtackVar.push_back(p);
        }
        if (posX + 1 == tempX  && posY == tempY && Game::getObject().figure[k]->getSide() != this->getSide() && Game::getObject().figure[k]->getPiece() != Piece::KING ) {
            p.x = tempX; p.y = tempY;
            this->allowingPointsToAtackVar.push_back(p);
        }
        if (posX - 1 == tempX  && posY == tempY && Game::getObject().figure[k]->getSide() != this->getSide() && Game::getObject().figure[k]->getPiece() != Piece::KING ) {
            p.x = tempX; p.y = tempY;
            this->allowingPointsToAtackVar.push_back(p);
        }
        if (posX  == tempX  && posY - 1 == tempY && Game::getObject().figure[k]->getSide() != this->getSide() && Game::getObject().figure[k]->getPiece() != Piece::KING ) {
            p.x = tempX; p.y = tempY;
            this->allowingPointsToAtackVar.push_back(p);
        }
        if (posX  == tempX  && posY + 1 == tempY && Game::getObject().figure[k]->getSide() != this->getSide() && Game::getObject().figure[k]->getPiece() != Piece::KING ) {
            p.x = tempX; p.y = tempY;
            this->allowingPointsToAtackVar.push_back(p);
        }
    }

    if (posX + 1 >= 0  && posX + 1 < 8 && posY + 1 >= 0 && posY + 1 < 8 && Game::getObject().board[posX + 1][posY + 1].empty) {
        p.x = posX + 1; p.y = posY + 1;
        vecTemp.push_back(p);
    }
    if (posX - 1 >= 0 && posX - 1 < 8 && posY - 1 >= 0 && posY - 1 < 8 && Game::getObject().board[posX - 1][posY - 1].empty) {
        p.x = posX - 1; p.y = posY - 1;
        vecTemp.push_back(p);
    }
    if (posX + 1 >= 0 && posX + 1 < 8 && posY - 1 >= 0 && posY - 1 < 8 && Game::getObject().board[posX + 1][posY - 1].empty) {
        p.x = posX + 1; p.y = posY - 1;
        vecTemp.push_back(p);
    }
    if (posX - 1 >= 0 && posX - 1 < 8 && posY + 1 >= 0 && posY + 1  < 8 && Game::getObject().board[posX - 1][posY + 1].empty) {
        p.x = posX - 1; p.y = posY + 1;
        vecTemp.push_back(p);
    }


    if (posX - 1 >= 0 && Game::getObject().board[posX - 1][posY].empty) {
        p.x = posX - 1; p.y = posY;
        vecTemp.push_back(p);
    }
    if (posX + 1 < 8 && Game::getObject().board[posX + 1][posY].empty) {
        p.x = posX + 1; p.y = posY;
        vecTemp.push_back(p);
    }
    if (posY - 1 >= 0 && Game::getObject().board[posX][posY - 1].empty) {
        p.x = posX; p.y = posY - 1;
        vecTemp.push_back(p);
    }
    if (posY + 1 < 8 && Game::getObject().board[posX][posY + 1].empty) {
        p.x = posX; p.y = posY + 1;
        vecTemp.push_back(p);
    }

    return vecTemp;
}
void King::eventTouchFigure() {
    handlerEventTouchFigure(this);
}

bool King::moveTO(int x, int y) {}

Side& King::getSide() {
    return this->side;
}
spSprite& King::getSprite() {
    return spriteKing;
}

void King::setCheckAtack(bool atack) {
    this->atack = atack;
}
bool King::atacked() {
    return this->atack;
}


//////////////////////////////////////////////////////////////////////
////////////////////////////////*Слон*//////////////////////////////
/////////////////////////////////////////////////////////////////////
Point Bishop::getIndexFigure() {
    Point p;
    for (int i = 0, y = 0; i <= 315; i += 45, y++) {
        for (int j = 45, x = 0; j <=360; j += 45, x++ ) {
            if (this->getSprite()->getPosition().x == i && this->getSprite()->getPosition().y == j) {
                 p.x = this->xFigure = x; p.y = this->yFigure = y;
            }
        }
    }
    return p;
}

bool& Bishop::pressed() {
    return this->press;
}
Piece Bishop::getPiece() {
    return Piece::BISHOP;
}
void Bishop::load_sprite(float x, float y, const Side& side)  {
    this->side = side;
    spriteBishop = new Sprite;
    if (side == White)
       spriteBishop->setResAnim(res::ui.getResAnim("WBishop"));
    else
       spriteBishop->setResAnim(res::ui.getResAnim("BBishop"));
    spriteBishop->attachTo(getStage());
    spriteBishop->setPosition(x, y);
}

bool Bishop::moveTO(int x, int y) {}

Side& Bishop::getSide() {
    return this->side;
}

spSprite& Bishop::getSprite() {
    return spriteBishop;
}
vector<Point>& Bishop::getAllowingPointsToAtack() {
    return this->allowingPointsToAtackVar;
}

vector<Point> Bishop::getAllowingPointsToMove() {
    vector<Point> vecTemp;
    Point p;
    int posX = this->getIndexFigure().x,
        posY = this->getIndexFigure().y;
    Game::getObject().emptyElementsOfBoard();
    bool flag = true;

    auto gettingPointsToMove = [](int& x, int& y, Bishop& bishop, bool& flag, Point& p) {
        if (!Game::getObject().board[x][y].empty) {
            for (int k = 0; k < 32; k++) {
                int tempX = Game::getObject().figure[k]->getIndexFigure().x,
                    tempY = Game::getObject().figure[k]->getIndexFigure().y;
                if (tempX == x && tempY == y && Game::getObject().figure[k]->getSide() == bishop.getSide() || Game::getObject().figure[k]->getPiece() == Piece::KING ) flag =false;
                if (tempX == x && tempY == y && Game::getObject().figure[k]->getSide() != bishop.getSide() && Game::getObject().figure[k]->getPiece() != Piece::KING ) {
                    p.x = tempX; p.y = tempY;
                    bishop.allowingPointsToAtackVar.push_back(p);
                    flag = false;
                }
            }
        }
    };
    for (int x  = posX + 1, y = posY + 1; x < 8 && x >= 0 && y < 8 && y >= 0 && flag; x++, y++) {
        gettingPointsToMove(x, y, *this, flag, p);
    }
    flag = true;
    for (int x  = posX - 1, y = posY - 1; x < 8 && x >= 0 && y < 8 && y >= 0 && flag; x--, y--) {
        gettingPointsToMove(x, y, *this, flag, p);
    }
    flag = true;
    for (int x  = posX + 1, y = posY - 1; x < 8 && x >= 0 && y < 8 && y >= 0 && flag; x++, y--) {
        gettingPointsToMove(x, y, *this, flag, p);
    }
    flag = true;
    for (int x  = posX - 1, y = posY + 1; x < 8 && x >= 0 && y < 8 && y >= 0 && flag; x--, y++) {
        gettingPointsToMove(x, y, *this, flag, p);
    }

    for (int x  = posX + 1, y = posY + 1; x < 8 && x >= 0 && y < 8 && y >= 0 && Game::getObject().board[x][y].empty; x++, y++) {
        p.x = x; p.y = y;
        vecTemp.push_back(p);
    }
    for (int x  = posX - 1, y = posY - 1; x < 8 && x >= 0 && y < 8 && y >= 0 && Game::getObject().board[x][y].empty;  x--, y--) {
        p.x = x; p.y = y;
        vecTemp.push_back(p);
    }
    for (int x  = posX + 1, y = posY - 1; x < 8 && x >= 0 && y < 8 && y >= 0 && Game::getObject().board[x][y].empty; x++, y--) {
        p.x = x; p.y = y;
        vecTemp.push_back(p);
    }
    for (int x  = posX - 1, y = posY + 1; x < 8 && x >= 0 && y < 8 && y >= 0 && Game::getObject().board[x][y].empty; x--, y++) {
        p.x = x; p.y = y;
        vecTemp.push_back(p);
    }
    return vecTemp;
}
void Bishop::eventTouchFigure() {
    handlerEventTouchFigure(this);
}

void Bishop::setCheckAtack(bool atack) {
    this->atack = atack;
}
bool Bishop::atacked() {
    return this->atack;
}

//////////////////////////////////////////////////////////////////////
////////////////////////////////*Ладья*//////////////////////////////
/////////////////////////////////////////////////////////////////////

Point Rook::getIndexFigure() {
    Point p;
    for (int i = 0, y = 0; i <= 315; i += 45, y++) {
        for (int j = 45, x = 0; j <=360; j += 45, x++ ) {
            if (this->getSprite()->getPosition().x == i && this->getSprite()->getPosition().y == j) {
                 p.x = this->xFigure = x; p.y = this->yFigure = y;
            }
        }
    }
    return p;
}

bool& Rook::pressed() {
    return this->press;
}
Piece Rook::getPiece() {
    return Piece::ROOK;
}
void Rook::load_sprite(float x, float y, const Side& side)  {
    this->side = side;
    spriteRook= new Sprite;
    if (side == White)
       spriteRook->setResAnim(res::ui.getResAnim("WRook"));
    else
       spriteRook->setResAnim(res::ui.getResAnim("BRook"));
    spriteRook->attachTo(getStage());
    spriteRook->setPosition(x, y);
}

bool Rook::moveTO(int x, int y) {}

Side& Rook::getSide() {
    return this->side;
}
spSprite& Rook::getSprite() {
    return spriteRook;
}
vector<Point>& Rook::getAllowingPointsToAtack() {
    return this->allowingPointsToAtackVar;
}

vector<Point> Rook::getAllowingPointsToMove() {
    vector<Point> vecTemp;
    Point p;
    int posX = this->getIndexFigure().x,
        posY = this->getIndexFigure().y;
    Game::getObject().emptyElementsOfBoard();
    bool flag = true;

    auto gettingPointsToMove = [](Rook& bishop, bool& flag, Point& p,  int& paramX, int& paramY) {
        if (!Game::getObject().board[paramX][paramY].empty) {
            for (int k = 0; k < 32; k++) {
                int tempX = Game::getObject().figure[k]->getIndexFigure().x,
                    tempY = Game::getObject().figure[k]->getIndexFigure().y;
                if (tempX == paramX && tempY == paramY && Game::getObject().figure[k]->getSide() == bishop.getSide()) flag = false;
                if (tempX == paramX && tempY == paramY && Game::getObject().figure[k]->getSide() != bishop.getSide() && Game::getObject().figure[k]->getPiece() != Piece::KING ) {
                    p.x = tempX; p.y = tempY;
                    bishop.allowingPointsToAtackVar.push_back(p);
                    flag = false;
                }
            }
        }
    };

    for (int x = posX - 1; x >= 0 && flag; x--) {
        gettingPointsToMove(*this, flag, p,  x, posY);
    }

    flag = true;
    for (int x = posX + 1; x < 8 && flag; x++) {
        gettingPointsToMove(*this, flag, p,  x, posY);
    }
    flag = true;
    for (int y = posY - 1; y >= 0 && flag; y--) {
        gettingPointsToMove(*this, flag, p,  posX, y);

    }
    flag = true;
    for (int y = posY + 1;  y < 8 && flag; y++) {
        gettingPointsToMove(*this, flag, p,  posX, y);
    }

    for (int x = posX - 1; x >= 0 && Game::getObject().board[x][posY].empty; x-- ) {
        p.x = x; p.y = posY;
        vecTemp.push_back(p);
    }
    for (int x = posX + 1; x < 8 && Game::getObject().board[x][posY].empty;  x++ ) {
        p.x = x; p.y = posY;
        vecTemp.push_back(p);
    }
    for (int y = posY - 1; y >= 0 && Game::getObject().board[posX][y].empty; y-- ) {
        p.x = posX; p.y = y;
        vecTemp.push_back(p);
    }
    for (int y = posY + 1; y <  8 && Game::getObject().board[posX][y].empty; y++ ) {
        p.x = posX; p.y = y;
        vecTemp.push_back(p);
    }
    return vecTemp;
}
void Rook::eventTouchFigure() {
    handlerEventTouchFigure(this);
}

void Rook::setCheckAtack(bool atack) {
    this->atack = atack;
}
bool Rook::atacked() {
    return this->atack;
}
//////////////////////////////////////////////////////////////////////
////////////////////////////////*Конь*//////////////////////////////
/////////////////////////////////////////////////////////////////////
Point Knight::getIndexFigure() {
    Point p;
    for (int i = 0, y = 0; i <= 315; i += 45, y++) {
        for (int j = 45, x = 0; j <=360; j += 45, x++ ) {
            if (this->getSprite()->getPosition().x == i && this->getSprite()->getPosition().y == j) {
                 p.x = this->xFigure = x; p.y = this->yFigure = y;
            }
        }
    }
    return p;
}


bool& Knight::pressed() {
    return this->press;
}
Piece Knight::getPiece() {
    return Piece::KNIGHT;
}
void Knight::load_sprite(float x, float y, const Side& side)  {
    this->side = side;
    spriteKnight= new Sprite;
    if (side == White)
       spriteKnight->setResAnim(res::ui.getResAnim("WKnight"));
    else
       spriteKnight->setResAnim(res::ui.getResAnim("BKnight"));
    spriteKnight->attachTo(getStage());
    spriteKnight->setPosition(x, y);
}

bool Knight::moveTO(int x, int y) {}

Side& Knight::getSide() {
    return this->side;
}

spSprite& Knight::getSprite() {
    return spriteKnight;
}

vector<Point>& Knight::getAllowingPointsToAtack() {
    return this->allowingPointsToAtackVar;
}

vector<Point> Knight::getAllowingPointsToMove() {
    vector<Point> tempVec;
    Point p;
    Game::getObject().emptyElementsOfBoard();
    int posX = this->getIndexFigure().x,
        posY = this->getIndexFigure().y;


    for (int k = 0; k < 32; k++) {
        int tempX = Game::getObject().figure[k]->getIndexFigure().x,
            tempY = Game::getObject().figure[k]->getIndexFigure().y;

        if (posX - 2  == tempX && posY - 1 ==  tempY && this->getSide() != Game::getObject().figure[k]->getSide() && Game::getObject().figure[k]->getPiece() != Piece::KING) {
            p.x = tempX; p.y = tempY;
           this->allowingPointsToAtackVar.push_back(p);
        }
        if (posX + 2  == tempX && posY + 1 ==  tempY && this->getSide() != Game::getObject().figure[k]->getSide() && Game::getObject().figure[k]->getPiece() != Piece::KING) {
            p.x = tempX; p.y = tempY;
           this->allowingPointsToAtackVar.push_back(p);
        }
        if (posX + 2  == tempX && posY - 1 ==  tempY && this->getSide() != Game::getObject().figure[k]->getSide() && Game::getObject().figure[k]->getPiece() != Piece::KING) {
            p.x = tempX; p.y = tempY;
           this->allowingPointsToAtackVar.push_back(p);
        }
        if (posX - 2  == tempX && posY + 1 ==  tempY && this->getSide() != Game::getObject().figure[k]->getSide() && Game::getObject().figure[k]->getPiece() != Piece::KING) {
            p.x = tempX; p.y = tempY;
           this->allowingPointsToAtackVar.push_back(p);
        }
        if (posX - 1  == tempX && posY - 2 ==  tempY && this->getSide() != Game::getObject().figure[k]->getSide() && Game::getObject().figure[k]->getPiece() != Piece::KING) {
            p.x = tempX; p.y = tempY;
           this->allowingPointsToAtackVar.push_back(p);
        }
        if (posX + 1  == tempX && posY + 2 ==  tempY && this->getSide() != Game::getObject().figure[k]->getSide() && Game::getObject().figure[k]->getPiece() != Piece::KING) {
            p.x = tempX; p.y = tempY;
           this->allowingPointsToAtackVar.push_back(p);
        }
        if (posX + 1  == tempX && posY - 2 ==  tempY && this->getSide() != Game::getObject().figure[k]->getSide() && Game::getObject().figure[k]->getPiece() != Piece::KING) {
            p.x = tempX; p.y = tempY;
           this->allowingPointsToAtackVar.push_back(p);
        }
        if (posX - 1  == tempX && posY + 2 ==  tempY && this->getSide() != Game::getObject().figure[k]->getSide() && Game::getObject().figure[k]->getPiece() != Piece::KING) {
            p.x = tempX; p.y = tempY;
           this->allowingPointsToAtackVar.push_back(p);
        }
    }

    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            Point p;
            if ((i == posX - 2 && posY + 1 == j) && Game::getObject().board[i][j].empty ) {             
               p.x = i; p.y = j;
               tempVec.push_back(p);
            }

            if ((i == posX - 2 && posY - 1 == j) && Game::getObject().board[i][j].empty) {
               p.x = i; p.y = j;
               tempVec.push_back(p);
            }
            if ((i == posX + 2 && posY + 1 == j) && Game::getObject().board[i][j].empty) {
               p.x = i; p.y = j;
               tempVec.push_back(p);
            }

            if ((i == posX + 2 && posY - 1 == j) && Game::getObject().board[i][j].empty) {
               p.x = i; p.y = j;
               tempVec.push_back(p);
            }

            if ((i == posX - 1 && posY + 2 == j) && Game::getObject().board[i][j].empty) {
               p.x = i; p.y = j;
               tempVec.push_back(p);
            }
            if ((i == posX - 1 && posY - 2 == j) && Game::getObject().board[i][j].empty ) {
               p.x = i; p.y = j;
               tempVec.push_back(p);
            }
            if ((i == posX + 1 && posY - 2 == j) && Game::getObject().board[i][j].empty) {
               p.x = i; p.y = j;
               tempVec.push_back(p);
            }
            if ((i == posX + 1 && posY + 2== j) && Game::getObject().board[i][j].empty) {
               p.x = i; p.y = j;
               tempVec.push_back(p);
            }

        }
    }
    return tempVec;
}
void Knight::eventTouchFigure() {
    handlerEventTouchFigure(this);
}


void Knight::setCheckAtack(bool atack) {
    this->atack = atack;
}
bool Knight::atacked() {
    return this->atack;
}

//////////////////////////////////////////////////////////////////////
////////////////////////////////*Пешка*//////////////////////////////
/////////////////////////////////////////////////////////////////////
Point Pawn::getIndexFigure() {
    Point p;
    for (int i = 0, y = 0; i <= 315; i += 45, y++) {
        for (int j = 45, x = 0; j <=360; j += 45, x++ ) {
            if (this->getSprite()->getPosition().x == i && this->getSprite()->getPosition().y == j) {
                p.x = this->xFigure = x; p.y = this->yFigure = y;
            }
        }
    }
    return p;
}


bool& Pawn::pressed() {
    return this->press;
}

Piece Pawn::getPiece() {
    return Piece::PAWN;
}

void Pawn::load_sprite(float x, float y, const Side& side)  {
    this->side = side;
    spritePawn= new Sprite;
    if (side == White)
       spritePawn->setResAnim(res::ui.getResAnim("WPawn"));
    else
       spritePawn->setResAnim(res::ui.getResAnim("BPawn"));
    spritePawn->attachTo(getStage());
    spritePawn->setPosition(x, y);
}

void Pawn::eventTouchFigure() {
    handlerEventTouchFigure(this);
}
bool Pawn::moveTO(int x, int y) {

}

vector<Point>& Pawn::getAllowingPointsToAtack() {
     return allowingPointsToAtackVar;
}
vector<Point> Pawn::getAllowingPointsToMove() {
    vector<Point> tempVec;
    Point p;
    int posX = this->getIndexFigure().x,
        posY = this->getIndexFigure().y;
    Game::getObject().emptyElementsOfBoard();
    if (this->getSide() == White)  {
        for (int k = 0; k < 32; k++) {
            int tempX = Game::getObject().figure[k]->getIndexFigure().x,
                tempY = Game::getObject().figure[k]->getIndexFigure().y;
            if (Game::getObject().figure[k]->getPiece() != Piece::KING && Game::getObject().figure[k]->getSide() != this->getSide() && tempX == posX - 1 && tempY == posY - 1) {
                Point tempP;
                tempP.x = tempX; tempP.y = tempY;
                allowingPointsToAtackVar.push_back(tempP);
            }
            if (Game::getObject().figure[k]->getPiece() != Piece::KING && Game::getObject().figure[k]->getSide() != this->getSide() && tempX == posX - 1 && tempY == posY + 1) {
                Point tempP;
                tempP.x = tempX; tempP.y = tempY;
                allowingPointsToAtackVar.push_back(tempP);
            }
        }

        if (this->isMoveCheck) {
            if (Game::getObject().board[posX - 1][posY].empty && (posX - 1) >= 0) {
                p.x = posX - 1; p.y = posY;
                tempVec.push_back(p);
            }
        } else {
            if (Game::getObject().board[posX - 2][posY].empty && Game::getObject().board[posX - 1][posY].empty && (posX - 1) >= 0) {
                p.x = posX - 2; p.y = posY;
                tempVec.push_back(p);
            }
            if (Game::getObject().board[posX - 1][posY].empty && (posX - 1) >= 0) {
                p.x = posX - 1; p.y = posY;
                tempVec.push_back(p);
            }
        }
    } else {

        for (int k = 0; k < 32; k++) {
            int tempX = Game::getObject().figure[k]->getIndexFigure().x,
                tempY = Game::getObject().figure[k]->getIndexFigure().y;
            if (Game::getObject().figure[k]->getPiece() != Piece::KING && Game::getObject().figure[k]->getSide() != this->getSide() && tempX == posX + 1 && tempY == posY - 1) {
                Point tempP;
                tempP.x = tempX; tempP.y = tempY;
                allowingPointsToAtackVar.push_back(tempP);
            }
            if (Game::getObject().figure[k]->getPiece() != Piece::KING && Game::getObject().figure[k]->getSide() != this->getSide() && tempX == posX + 1 && tempY == posY + 1) {
                Point tempP;
                tempP.x = tempX; tempP.y = tempY;
                allowingPointsToAtackVar.push_back(tempP);
            }
        }

        if (this->isMoveCheck) {
            if (Game::getObject().board[posX + 1][posY].empty && (posX + 1) < 8) {
                p.x = posX + 1; p.y = posY;
                tempVec.push_back(p);
            }
        } else {
            if (Game::getObject().board[posX + 2][posY].empty && Game::getObject().board[posX + 1][posY].empty && (posX + 1) < 8) {
                p.x = posX + 2; p.y = posY;
                tempVec.push_back(p);
            }
            if (Game::getObject().board[posX + 1][posY].empty && (posX + 1) < 8) {
                p.x = posX + 1; p.y = posY;
                tempVec.push_back(p);
            }
        }
    }

    return tempVec;
}

Side& Pawn::getSide() {
    return this->side;
}



spSprite& Pawn::getSprite() {
    return spritePawn;
}

void Pawn::setCheckAtack(bool atack) {
    this->atack = atack;
}
bool Pawn::atacked() {
    return this->atack;
}

void Pawn::setCheckFirstMove(bool isMoveCheck) {
    this->isMoveCheck = isMoveCheck;
}

